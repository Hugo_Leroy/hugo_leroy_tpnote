﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Compteur
    {
        int compteur = 0;

        public int increment()
        {
            compteur = compteur+1;
            return compteur;
        }

        public int decrement()
        {
            compteur = compteur-1;

            if (compteur <= 0)
            {
                compteur = 0;
            }
            return compteur;
        }

        public int raz()
        {
            compteur = 0;
            return compteur;
        }
    }
}
