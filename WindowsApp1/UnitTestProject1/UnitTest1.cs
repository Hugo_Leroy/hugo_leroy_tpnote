﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testincrement()
        {
            Assert.AreEqual(1, increment);
        }

        [TestMethod]
        public void Testdecrement()
        {
            Assert.AreEqual(-1, decrement);
        }

        [TestMethod]
        public void Testraz()
        {
            Assert.AreEqual(0, raz);
        }
    }
}
