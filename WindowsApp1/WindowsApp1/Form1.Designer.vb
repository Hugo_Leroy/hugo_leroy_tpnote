﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class basiccounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.decrement = New System.Windows.Forms.Button()
        Me.increment = New System.Windows.Forms.Button()
        Me.raz = New System.Windows.Forms.Button()
        Me.total = New System.Windows.Forms.Label()
        Me.nbr = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'decrement
        '
        Me.decrement.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.decrement.Location = New System.Drawing.Point(12, 121)
        Me.decrement.Name = "decrement"
        Me.decrement.Size = New System.Drawing.Size(85, 65)
        Me.decrement.TabIndex = 0
        Me.decrement.Text = "-"
        Me.decrement.UseVisualStyleBackColor = True
        '
        'increment
        '
        Me.increment.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.increment.Location = New System.Drawing.Point(291, 121)
        Me.increment.Name = "increment"
        Me.increment.Size = New System.Drawing.Size(92, 65)
        Me.increment.TabIndex = 1
        Me.increment.Text = "+"
        Me.increment.UseVisualStyleBackColor = True
        '
        'raz
        '
        Me.raz.Location = New System.Drawing.Point(150, 226)
        Me.raz.Name = "raz"
        Me.raz.Size = New System.Drawing.Size(75, 23)
        Me.raz.TabIndex = 2
        Me.raz.Text = "RAZ"
        Me.raz.UseVisualStyleBackColor = True
        '
        'total
        '
        Me.total.AutoSize = True
        Me.total.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total.Location = New System.Drawing.Point(143, 43)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(94, 37)
        Me.total.TabIndex = 3
        Me.total.Text = "Total"
        '
        'nbr
        '
        Me.nbr.AutoSize = True
        Me.nbr.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nbr.Location = New System.Drawing.Point(187, 144)
        Me.nbr.Name = "nbr"
        Me.nbr.Size = New System.Drawing.Size(0, 25)
        Me.nbr.TabIndex = 4
        '
        'basiccounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(412, 310)
        Me.Controls.Add(Me.nbr)
        Me.Controls.Add(Me.total)
        Me.Controls.Add(Me.raz)
        Me.Controls.Add(Me.increment)
        Me.Controls.Add(Me.decrement)
        Me.Name = "basiccounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents decrement As Button
    Friend WithEvents increment As Button
    Friend WithEvents raz As Button
    Friend WithEvents total As Label
    Friend WithEvents nbr As Label
End Class
